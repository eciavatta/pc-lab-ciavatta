import asyncio
from random import randrange, random

from quart import Quart, jsonify

app = Quart(__name__)

temperature = 0
external_temperature = 0
heating_enabled = False
cooling_enabled = False

ambient_delta_variation = 0.1
system_delta_variation = 0.5

min_temperature = 0
max_temperature = 40


async def setup():
    global temperature
    print('Setting up thermostat')
    temperature = 20.0
    set_random_external_temperature()


async def run():
    global temperature

    while True:
        ambient_delta = ambient_delta_variation if external_temperature > temperature else -ambient_delta_variation
        if heating_enabled:
            system_delta = system_delta_variation
        elif cooling_enabled:
            system_delta = -system_delta_variation
        else:
            system_delta = 0

        temperature = min(temperature + ambient_delta + system_delta, max_temperature)
        temperature = max(temperature, min_temperature)

        if random() > 0.95:
            set_random_external_temperature()

        print(f"temperature: {temperature:.1f} *C")
        await asyncio.sleep(3)


def set_random_external_temperature():
    global external_temperature
    external_temperature = randrange(0, 400) / 10
    print(f"external temperature: {external_temperature:.1f} *C")


@app.route("/api/properties/temperature", methods = ['GET'])
async def get_temperature():
    return jsonify({'temperature': temperature})


@app.route("/api/actions/startHeating", methods = ['POST'])
async def start_heating():
    change_heating(True)
    change_cooling(False)
    return jsonify({'message': 'ok'})


@app.route("/api/actions/stopHeating", methods = ['POST'])
async def stop_heating():
    change_heating(False)
    return jsonify({'message': 'ok'})


@app.route("/api/actions/startCooling", methods = ['POST'])
async def start_cooling():
    change_cooling(True)
    change_heating(False)
    return jsonify({'message': 'ok'})


@app.route("/api/actions/stopCooling", methods = ['POST'])
async def stop_cooling():
    change_cooling(False)
    return jsonify({'message': 'ok'})


def change_heating(status):
    global heating_enabled

    if heating_enabled != status:
        heating_enabled = status
        print(f'[*] {"start" if status else "stop"} heating')


def change_cooling(status):
    global cooling_enabled

    if cooling_enabled != status:
        cooling_enabled = status
        print(f'[*] {"start" if status else "stop"} cooling')


async def main():
    await setup()
    await run()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main())
    app.run()
