import time

import requests

thermostat_url = '127.0.0.1:5000/api'
temperature_threshold = 0.5


def run():
    answer = 0
    while answer != 4:
        print("Choose an option:")
        print("  [1] set temperature")
        print("  [2] set and maintain temperature")
        print("  [3] set, maintain and control temperature")
        print("  [4] bye bye")

        answer = int(input("> "))
        if answer == 1:
            temperature = ask_desired_temperature()
            reached = reach_temperature(temperature)
            print(f"Temperature reached is {current_temperature():.1f} *C (reached: {reached})")
        elif answer == 2:
            temperature = ask_desired_temperature()
            try:
                while reach_temperature(temperature):
                    print(f"Current temperature is {current_temperature():.1f} *C")
                    time.sleep(10)
            finally:
                print(f"Temperature reached is {current_temperature():.1f} *C")
        elif answer == 3:
            temperature = ask_desired_temperature()
            while temperature > 0:
                try:
                    while reach_temperature(temperature):
                        print(f"Current temperature is {current_temperature():.1f} *C")
                        time.sleep(10)
                finally:
                    print(f"Temperature reached is {current_temperature():.1f} *C")

                temperature = ask_desired_temperature("< 0 to stop")
        else:
            answer = 4


def ask_desired_temperature(optional_message = None):
    return float(input(f"Desired temperature{' (' + optional_message + ') ' if optional_message else ''}: "))


def current_temperature():
    return requests.get(f'http://{thermostat_url}/properties/temperature').json()['temperature']


def reach_temperature(t):
    actual = current_temperature()
    if abs(actual - t) < temperature_threshold:
        return True

    need_heating = actual < t

    try:
        if need_heating:
            requests.post(f'http://{thermostat_url}/actions/startHeating')
        else:
            requests.post(f'http://{thermostat_url}/actions/startCooling')

        while abs(actual - t) > temperature_threshold:
            time.sleep(1)
            actual = current_temperature()
    except KeyboardInterrupt:
        return False
    finally:
        if need_heating:
            requests.post(f'http://{thermostat_url}/actions/stopHeating')
        else:
            requests.post(f'http://{thermostat_url}/actions/stopCooling')

    return True


if __name__ == '__main__':
    print(f"Current temperature is {current_temperature():.1f} *C")
    run()
